<?php

class Seed002LanguagesSeeder extends AbstractSeeder {

	public function getTableName(): string {
		return "languages";
	}

	public function getData(): array {
		return [
			["id" => 1, "name" => "angielski", "symbol" => "gb"],
			["id" => 2, "name" => "polski", "symbol" => "pl"],
			["id" => 3, "name" => "rosyjski", "symbol" => "ru"],
			["id" => 4, "name" => "chiński", "symbol" => "cn"],
		];
	}
}

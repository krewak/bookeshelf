<?php

class Seed004AuthorsSeeder extends AbstractSeeder {

	public function getTableName(): string {
		return "authors";
	}

	public function getData(): array {
		return [
			[
				"id" => 1,
				"first_name" => "Michael A.",
				"last_name" => "Stackpole",
				"language_id" => 1,
				"portrait_filename" => "michaelastackpole.png",
			],
			[
				"id" => 2,
				"first_name" => "Philip K.",
				"last_name" => "Dick",
				"language_id" => 1,
				"portrait_filename" => "philipkdick.png",
			],
			[
				"id" => 3,
				"first_name" => "Roger",
				"last_name" => "Zelazny",
				"language_id" => 1,
				"portrait_filename" => "rogerzelazny.png",
			],
			[
				"id" => 4,
				"first_name" => "Cormac",
				"last_name" => "McCarthy",
				"language_id" => 1,
				"portrait_filename" => "cormacmccarthy.png",
			],
			[
				"id" => 5,
				"first_name" => "Arthur C.",
				"last_name" => "Clarke",
				"language_id" => 1,
				"portrait_filename" => "arthurcclarke.png",
			],
			[
				"id" => 6,
				"first_name" => "Blake",
				"last_name" => "Crouch",
				"language_id" => 1,
				"portrait_filename" => "blakecrouch.png",
			],
			[
				"id" => 7,
				"first_name" => "Filip",
				"last_name" => "Springer",
				"language_id" => 2,
				"portrait_filename" => "filipspringer.png",
			],
			[
				"id" => 8,
				"first_name" => "Dmitry",
				"last_name" => "Glukhovsky",
				"language_id" => 3,
				"portrait_filename" => "dmitryglukhovsky.png",
			],
			[
				"id" => 9,
				"first_name" => "Peter V.",
				"last_name" => "Brett",
				"language_id" => 1,
				"portrait_filename" => "petervbrett.png",
			],
			[
				"id" => 10,
				"first_name" => "Ray",
				"last_name" => "Bradbury",
				"language_id" => 1,
				"portrait_filename" => "raybradbury.png",
			],
			[
				"id" => 11,
				"first_name" => "Michael",
				"last_name" => "Chabon",
				"language_id" => 1,
				"portrait_filename" => "michaelchabon.png",
			],
			[
				"id" => 12,
				"first_name" => "Jeremy",
				"last_name" => "Clarkson",
				"language_id" => 1,
				"portrait_filename" => "jeremyclarkson.png",
			],
			[
				"id" => 13,
				"first_name" => "Cixin",
				"last_name" => "Liu",
				"language_id" => 4,
				"portrait_filename" => "liucixin.png",
			],
			[
				"id" => 14,
				"first_name" => "Stuart",
				"last_name" => "Moore",
				"language_id" => 1,
				"portrait_filename" => "stuartmoore.png",
			],
			[
				"id" => 15,
				"first_name" => "James",
				"last_name" => "Luceno",
				"language_id" => 1,
				"portrait_filename" => "jamesluceno.png",
			],
			[
				"id" => 16,
				"first_name" => "Matthew W.",
				"last_name" => "Stover",
				"language_id" => 1,
				"portrait_filename" => "matthewstover.png",
			],
			[
				"id" => 17,
				"first_name" => "Peter",
				"last_name" => "Watts",
				"language_id" => 1,
				"portrait_filename" => "peterwatts.png",
			],
		];
	}
}

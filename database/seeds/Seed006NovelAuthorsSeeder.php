<?php

class Seed006NovelAuthorsSeeder extends AbstractSeeder {

	public function getTableName(): string {
		return "novel_authors";
	}

	public function getData(): array {
		return [
			["novel_id" => 1, "author_id" => 1],
			["novel_id" => 2, "author_id" => 2],
			["novel_id" => 2, "author_id" => 3],
			["novel_id" => 3, "author_id" => 4],
			["novel_id" => 4, "author_id" => 5],
			["novel_id" => 5, "author_id" => 5],
			["novel_id" => 6, "author_id" => 5],
			["novel_id" => 7, "author_id" => 5],
			["novel_id" => 8, "author_id" => 6],
			["novel_id" => 9, "author_id" => 7],
			["novel_id" => 10, "author_id" => 8],
			["novel_id" => 11, "author_id" => 9],
			["novel_id" => 12, "author_id" => 10],
			["novel_id" => 13, "author_id" => 11],
			["novel_id" => 14, "author_id" => 12],
			["novel_id" => 15, "author_id" => 13],
			["novel_id" => 16, "author_id" => 14],
			["novel_id" => 17, "author_id" => 15],
			["novel_id" => 18, "author_id" => 16],
			["novel_id" => 19, "author_id" => 17],
		];
	}
}

<?php

class Seed001DevicesSeeder extends AbstractSeeder {

	public function getTableName(): string {
		return "devices";
	}

	public function getData(): array {
		return [
			["id" => 1,	"name" => "Kindle Paperwhite 3"],
			["id" => 2,	"name" => "miękka okładka"],
		];
	}
}

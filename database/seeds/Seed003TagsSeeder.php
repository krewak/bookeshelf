<?php

class Seed003TagsSeeder extends AbstractSeeder {

	public function getTableName(): string {
		return "tags";
	}

	public function getData(): array {
		return [
			["id" => 1, "name" => "literatura piękna"],
			["id" => 2, "name" => "literatura faktu"],
			["id" => 3, "name" => "fantastyka naukowa"],
			["id" => 4, "name" => "fantasy"],
			["id" => 5, "name" => "Gwiezdne wojny"],
			["id" => 6, "name" => "Gwiezdne wojny - Expanded Universe"],
			["id" => 7, "name" => "Gwiezdne wojny - X-wingi"],
			["id" => 8, "name" => "wojenne"],
			["id" => 9, "name" => "postapokalipsa"],
			["id" => 10, "name" => "powieść drogi"],
			["id" => 11, "name" => "zdobywca Nagrody Pulitzera"],
			["id" => 12, "name" => "tetralogia Odyseja kosmiczna"],
			["id" => 13, "name" => "trylogia Wayward Pines"],
			["id" => 14, "name" => "thrillery"],
			["id" => 15, "name" => "kryminały"],
			["id" => 16, "name" => "trylogia Metro 2033"],
			["id" => 17, "name" => "Uniwersum Metro 2033"],
			["id" => 18, "name" => "Cykl demoniczny"],
			["id" => 19, "name" => "historia alternatywna"],
			["id" => 20, "name" => "zdobywca Nagrody Hugo"],
			["id" => 21, "name" => "zdobywca Nagrody Nebula"],
			["id" => 22, "name" => "zdobywca Nagrody Locusa"],
			["id" => 23, "name" => "felietony"],
			["id" => 24, "name" => "Świat według Clarksona"],
			["id" => 25, "name" => "trylogia Wspomnienie o przeszłości Ziemi"],
			["id" => 26, "name" => "Marvel Universe"],
			["id" => 27, "name" => "superbohaterowie"],
			["id" => 28, "name" => "adaptacje komiksów"],
			["id" => 29, "name" => "trylogia Mrocznego Lorda"],
			["id" => 30, "name" => "adaptacje filmów"],
			["id" => 31, "name" => "Gwiezdne wojny - adaptacje filmów"],
			["id" => 32, "name" => "sztuczna inteligencja"],
			["id" => 33, "name" => "wampiry"],
			["id" => 34, "name" => "pierwszy kontakt"],
			["id" => 35, "name" => "dylogia Firefall"],
		];
	}
}

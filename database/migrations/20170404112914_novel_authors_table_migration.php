<?php

use Phinx\Migration\AbstractMigration as Migration;

class NovelAuthorsTableMigration extends Migration {

	public function change() {
		$table = $this->table("novel_authors");

		$table->addColumn("novel_id", "integer");
		$table->addForeignKey("novel_id", "novels", "id", ["delete"=> "CASCADE", "update"=> "NO_ACTION"]);

		$table->addColumn("author_id", "integer");
		$table->addForeignKey("author_id", "authors", "id", ["delete"=> "CASCADE", "update"=> "NO_ACTION"]);

		$table->create();
	}

}

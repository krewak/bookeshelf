<?php

use Phinx\Migration\AbstractMigration as Migration;

class LanguagesTableMigration extends Migration {

	public function change() {
		$table = $this->table("languages");
		$table->addColumn("name", "string");
		$table->addColumn("symbol", "string");
		$table->create();
	}

}

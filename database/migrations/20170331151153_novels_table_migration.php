<?php

use Phinx\Migration\AbstractMigration as Migration;

class NovelsTableMigration extends Migration {

	public function change() {
		$table = $this->table("novels");

		$table->addColumn("title", "string");
		$table->addColumn("original_title", "string", ["default" => ""]);

		$table->addColumn("language_id", "integer");
		$table->addForeignKey("language_id", "languages", "id", ["delete" => "CASCADE", "update" => "NO_ACTION"]);
		$table->addColumn("original_language_id", "integer");
		$table->addForeignKey("original_language_id", "languages", "id", ["delete" => "CASCADE", "update" => "NO_ACTION"]);
		
		$table->addColumn("device_id", "integer");
		$table->addForeignKey("device_id", "devices", "id", ["delete" => "CASCADE", "update" => "NO_ACTION"]);

		$table->addColumn("publication_year", "integer", ["null" => true, "default" => null]);
		$table->addColumn("pages", "integer");
		$table->addColumn("cover_filename", "string", ["default" => ""]);
		$table->addColumn("read_at", "date", ["null" => true, "default" => null]);
		$table->addColumn("reread", "boolean", ["default" => false]);

		$table->addColumn("summary", "text");

		$table->addColumn("created_at", "timestamp", ["default" => "CURRENT_TIMESTAMP"]);
		$table->addColumn("updated_at", "timestamp", ["default" => "CURRENT_TIMESTAMP", "update" => "CURRENT_TIMESTAMP"]);

		$table->create();
	}

}

<?php

use Phinx\Migration\AbstractMigration as Migration;

class TagsTableMigration extends Migration {

	public function change() {
		$table = $this->table("tags");
		$table->addColumn("name", "string");
		$table->create();
	}

}

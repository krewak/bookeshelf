<?php

use Phinx\Migration\AbstractMigration as Migration;

class DevicesTableMigration extends Migration {

	public function change() {
		$table = $this->table("devices");
		$table->addColumn("name", "string");
		$table->create();
	}

}

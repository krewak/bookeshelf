<?php

use Phinx\Migration\AbstractMigration as Migration;

class NovelTagsTableMigration extends Migration {

	public function change() {
		$table = $this->table("novel_tags");

		$table->addColumn("novel_id", "integer");
		$table->addForeignKey("novel_id", "novels", "id", ["delete"=> "CASCADE", "update"=> "NO_ACTION"]);

		$table->addColumn("tag_id", "integer");
		$table->addForeignKey("tag_id", "tags", "id", ["delete"=> "CASCADE", "update"=> "NO_ACTION"]);

		$table->create();
	}

}

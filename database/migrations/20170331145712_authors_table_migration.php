<?php

use Phinx\Migration\AbstractMigration as Migration;

class AuthorsTableMigration extends Migration {

	public function change() {
		$table = $this->table("authors");
		
		$table->addColumn("first_name", "string");
		$table->addColumn("last_name", "string");

		$table->addColumn("language_id", "integer");
		$table->addForeignKey("language_id", "languages", "id", ["delete" => "CASCADE", "update" => "NO_ACTION"]);

		$table->addColumn("portrait_filename", "string", ["default" => ""]);

		$table->create();
	}

}

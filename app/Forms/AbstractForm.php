<?php

namespace Bookeshelf\Forms;

use Phalcon\Mvc\Model;

abstract class AbstractForm {

	public function getEditForm(Model $model): array {
		return $this->get();
	}

}

<?php

namespace Bookeshelf\Forms;

class NovelForm extends AbstractForm {

	protected function get() {
		return [
			[
				[
					"label" => "Tytuł książki",
					"name" => "title",
					"type" => "text-field",
					"value" => "Labirynt zła",
					"options" => [
						"type" => "text"
					]
				],
				[
					"label" => "Liczba stron",
					"name" => "pages",
					"type" => "text-field",
					"value" => "482",
					"options" => [
						"type" => "number"
					]
				],
				[
					"label" => "Język",
					"name" => "language",
					"type" => "dropdown-field",
					"value" => [2],
					"options" => [
						"isMultiple" => true,
						"isFlag" => true,
						"dataset" => [
							[ "label" => "angielski", "value" => 1, "i" => "gb" ],
							[ "label" => "chiński", "value" => 2, "i" => "cn" ],
							[ "label" => "polski", "value" => 3, "i" => "pl" ],
							[ "label" => "rosyjski", "value" => 4, "i" => "ru" ],
						]
					]
				],
				[
					"label" => "Autor",
					"name" => "author",
					"type" => "dropdown-field",
					"value" => [1, 3],
					"options" => [
						"isMultiple" => true,
						"isFlag" => false,
						"dataset" => [
							[ "label" => "James Luceno", "value" => 1, "i" => "user" ],
							[ "label" => "Troy Denning", "value" => 2, "i" => "user" ],
							[ "label" => "Karen Traviss", "value" => 3, "i" => "user" ],
							[ "label" => "Timothy Zahn", "value" => 4, "i" => "user" ],
						]
					]
				],
				[
					"label" => "Tagi",
					"name" => "tag",
					"type" => "dropdown-field",
					"value" => [1],
					"options" => [
						"isMultiple" => true,
						"isFlag" => false,
						"dataset" => [
							[ "label" => "literatura piękna", "value" => 1, "i" => "hashtag" ],
							[ "label" => "literatura faktu", "value" => 2, "i" => "hashtag" ],
							[ "label" => "fantastyka naukowa", "value" => 3, "i" => "hashtag" ],
						]
					]
				],
				[
					"label" => "Czytana ponownie?",
					"name" => "reread",
					"type" => "checkbox-field",
					"value" => "",
				],
			],
			[
				[
					"label" => "Opis",
					"name" => "summary",
					"type" => "text-area-field",
					"value" => "Lorem ipsum",
				],
				[
					"label" => "Okładka",
					"name" => "cover",
					"type" => "image-field",
					"value" => "",
				],
			]
		];
	}

}

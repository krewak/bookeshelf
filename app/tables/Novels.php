<?php

namespace Bookeshelf\Tables;

class Novels extends Table {

	protected $header = "Przeczytane książki";

	protected function setColumns(): void {
		$this->insertNewColumn()
			->setImageSource("cover")
			->setRoute("novel", "id");

		$this->insertNewColumn()
			->setLabel("tytuł", "tytuł oryginalny")
			->setStrongValue("title")
			->setSubTextValue("original_title")
			->setRoute("novel", "id")
			->enableSorting("title", "original_title");

		$this->insertNewColumn()
			->setLabel("autorzy")
			->setList("authors")
			->setRoute("author", "id")
			->enableSorting("first_author_lastname");

		$this->insertNewColumn()
			->setLabel("rok wyd.", "l. stron")
			->setTextValue("publication_year")
			->setSubTextValue("pages")
			->enableSorting("publication_year", "pages");

		$this->insertNewColumn()
			->setLabel("język", "język oryginalny")
			->setFlag("language", "original_language")
			->setRoute("language", "id")
			->enableSorting("language_name", "original_language_name");

		$this->insertNewColumn()
			->setLabel("data przeczytania", "relatywnie")
			->setTextValue("read_at_realdate")
			->setSubTextValue("read_at_dateforhuman")
			->enableSorting("read_at", "read_at");
			
		$this->insertNewColumn()
			->setLabel("nośnik", "inne informacje")
			->setTextValue("device_name")
			->setSubTextValue("reread")
			->setRoute("device", "device_id")
			->enableSorting("device_name", "reread");
	}

}

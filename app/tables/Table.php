<?php

namespace Bookeshelf\Tables;

use Iterator;

abstract class Table implements Iterator {

	protected $position = 0;
	protected $columns = [];
	protected $header = "";

	public function __construct() {
		$this->setColumns();
	}

	public function current(): TableColumn {
		return $this->columns[$this->position];
	}

	public function key(): int {
		return $this->position;
	}

	public function next(): void {
		++$this->position;
	}

	public function rewind(): void {
		$this->position = 0;
	}

	public function valid(): bool {
		return isset($this->columns[$this->position]);
	}

	public function insertNewColumn(): TableColumn {
		$column = new TableColumn();
		$this->columns[] = $column;
		return $column;
	}

	public function setHeader(string $header): self {
		$this->header = $header;
		return $this;
	}

	public function getHeader(): string {
		return $this->header;
	}

	public function getColumns(): array {
		return $this->columns;
	}

}
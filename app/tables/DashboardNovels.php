<?php

namespace Bookeshelf\Tables;

class DashboardNovels extends Table {

	protected $header = "Przeczytane książki";

	protected function setColumns(): void {
		$this->insertNewColumn()
			->setImageSource("cover");

		$this->insertNewColumn()
			->setLabel("tytuł")
			->setStrongValue("title");

		$this->insertNewColumn()
			->setLabel("autorzy")
			->setList("authors");

		$this->insertNewColumn()
			->setLabel("rok wyd.")
			->setTextValue("publication_year");

		$this->insertNewColumn()
			->setLabel("l. stron")
			->setTextValue("pages");

		$this->insertNewColumn()
			->setLabel("data przeczytania")
			->setTextValue("read_at_realdate");
			
		$this->insertNewColumn()
			->setLabel("nośnik")
			->setTextValue("device_name");
	}

}

<?php

namespace Bookeshelf\Tables;

class TableColumn {

	public $sorting_label = null;
	public $sorting_sublabel = null;

	public $label = "";
	public $sublabel = "";

	public $text_value = "";
	public $strong_value = "";
	public $sub_text_value = "";
	public $image_source = "";
	public $flag_symbol = "";

	public $is_flag = false;
	public $language = "";
	public $original_language = "";

	public $list = "";
	public $object = "";

	public $route_dataset = "";
	public $route_anchor = "";

	public function __toString(): string {
		return $this->label;
	}

	public function setLabel(string $label, string $sublabel = ""): self {
		$this->label = $label;

		if($sublabel) {
			$this->sublabel = $sublabel;
		}

		return $this;
	}

	public function enableSorting(string $label, string $sublabel = ""): self {
		$this->sorting_label = $label;

		if($sublabel) {
			$this->sorting_sublabel = $sublabel;
		}
		
		return $this;
	}

	public function setTextValue(string $text_value): self {
		$this->text_value = $text_value;
		return $this;
	}

	public function setStrongValue(string $strong_value): self {
		$this->strong_value = $strong_value;
		return $this;
	}

	public function setSubTextValue(string $sub_text_value): self {
		$this->sub_text_value = $sub_text_value;
		return $this;
	}

	public function setImageSource(string $image_source): self {
		$this->image_source = $image_source;
		return $this;
	}

	public function setList(string $list): self {
		$this->list = $list;
		return $this;
	}

	public function setFlagSymbol(string $flag_symbol): self {
		$this->flag_symbol = $flag_symbol;
		return $this;
	}

	public function setFlag(string $language, string $original_language = ""): self {
		$this->is_flag = true;
		$this->language = $language;
		$this->original_language = $original_language;
		return $this;
	}

	public function setRoute(string $dataset, string $anchor): self {
		$this->route_dataset = $dataset;
		$this->route_anchor = $anchor;
		return $this;
	}

}
<?php

namespace Bookeshelf\Tables;

class Devices extends Table {

	protected $header = "Nośniki książek";

	protected function setColumns(): void {
		$this->insertNewColumn()
			->setLabel("nazwa")
			->setStrongValue("name")
			->enableSorting("name")
			->setRoute("device", "id");

		$this->insertNewColumn()
			->setLabel("liczba książek")
			->setTextValue("tagged_novels")
			->enableSorting("tagged_novels");

		$this->insertNewColumn()
			->setLabel("ostatnio czytana powieść")
			->setTextValue("last_novel")
			->setRoute("novel", "last_novel_id")
			->enableSorting("last_novel");

		$this->insertNewColumn()
			->setLabel("kiedy przeczytana?")
			->setTextValue("last_novel_dateforhuman")
			->enableSorting("last_novel_date");
	}

}

<?php

namespace Bookeshelf\Tables;

class Authors extends Table {

	protected $header = "Autorzy przeczytanych książek";

	protected function setColumns(): void {
		$this->insertNewColumn()
			->setImageSource("portrait")
			->setRoute("author", "id");

		$this->insertNewColumn()
			->setLabel("nazwisko")
			->setStrongValue("last_name")
			->enableSorting("last_name")
			->setRoute("author", "id");

		$this->insertNewColumn()
			->setLabel("imię")
			->setTextValue("first_name")
			->enableSorting("first_name")
			->setRoute("author", "id");

		$this->insertNewColumn()
			->setLabel("język ojczysty")
			->setFlag("language")
			->setRoute("language", "id")
			->enableSorting("language_name");

		$this->insertNewColumn()
			->setLabel("liczba książek")
			->setTextValue("written_novels")
			->enableSorting("written_novels");

		$this->insertNewColumn()
			->setLabel("ostatnio czytana powieść")
			->setTextValue("last_novel")
			->setRoute("novel", "last_novel_id")
			->enableSorting("last_novel");

		$this->insertNewColumn()
			->setLabel("kiedy przeczytana?")
			->setTextValue("last_novel_dateforhuman")
			->enableSorting("last_novel_date");
	}

}

<?php

namespace Bookeshelf\Tables;

class Languages extends Table {

	protected $header = "Języki przeczytanych książek";

	protected function setColumns(): void {
		$this->insertNewColumn()
			->setFlagSymbol("symbol")
			->setRoute("language", "id");

		$this->insertNewColumn()
			->setLabel("nazwa")
			->setTextValue("name")
			->enableSorting("name")
			->setRoute("language", "id");

		$this->insertNewColumn()
			->setLabel("przeczytane książki")
			->setTextValue("read_novels")
			->enableSorting("read_novels");

		$this->insertNewColumn()
			->setLabel("książki w oryginale")
			->setTextValue("written_novels")
			->enableSorting("written_novels");
	}

}

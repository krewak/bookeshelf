<?php

namespace Bookeshelf\Exceptions;

use Exception;

class ObjectNotFound extends Exception {

	public function __construct() {
		$this->message = "Object not found.";
	}

}
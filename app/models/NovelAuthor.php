<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class NovelAuthor extends Model {

	public $id;
	public $novel_id;
	public $author_id;

	public function initialize(): void {
		$this->setSource("novel_authors");
		$this->belongsTo("novel_id", Novel::class, "id");
		$this->belongsTo("author_id", Author::class, "id");
	}
	
}

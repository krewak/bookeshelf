<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class Tag extends Model {

	public $id;
	public $name;

	public function __toString(): string {
		return $this->name;
	}

	public function initialize(): void {
		$this->setSource("tags");
		$this->hasManyToMany("id", NovelTag::class, "tag_id", "novel_id", Novel::class, "id", ["alias" => "Novels"]);
	}
	
}

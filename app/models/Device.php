<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class Device extends Model {

	public $id;
	public $name;

	public function __toString(): string {
		return $this->name;
	}

	public function initialize(): void {
		$this->setSource("devices");
		$this->hasMany("id", Novel::class, "device_id", ["alias" => "Novels"]);
	}
	
}

<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class Language extends Model {

	public $id;
	public $name;

	public function __toString(): string {
		return $this->name;
	}

	public function initialize(): void {
		$this->setSource("languages");
		$this->hasMany("id", Novel::class, "language_id", ["alias" => "ReadNovels"]);
		$this->hasMany("id", Novel::class, "original_language_id", ["alias" => "WrittenNovels"]);
	}
	
}

<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class Novel extends Model {

	public $id;
	public $title;
	public $original_title;
	public $language_id;
	public $original_language_id;
	public $publication_year;
	public $cover_filename;
	public $device_id;
	public $read_at;
	public $summary;

	public function initialize(): void {
		$this->setSource("novels");

		$this->hasMany("id", NovelAuthor::class, "novel_id", ["alias" => "NovelAuthors"]);
		$this->hasManyToMany("id", NovelAuthor::class, "novel_id", "author_id", Author::class, "id", ["alias" => "Authors"]);
		$this->hasManyToMany("id", NovelTag::class, "novel_id", "tag_id", Tag::class, "id", ["alias" => "Tags", "params" => ["order" => "name ASC"]]);
		
		$this->belongsTo("language_id", Language::class, "id", ["alias" => "Language"]);
		$this->belongsTo("original_language_id", Language::class, "id", ["alias" => "OriginalLanguage"]);
		$this->belongsTo("device_id", Device::class, "id", ["alias" => "Device"]);

		$this->allowEmptyStringValues(["read_at", "summary"]);
	}

	public function afterFetch(): void {
		$public = $this->getDI()->get("config")->application->publicDir;
		$path = $this->getDI()->get("config")->application->coversUri;

		if($this->cover_filename && file_exists($public . $path . $this->cover_filename)) {
			$this->cover_filename = $path . $this->cover_filename;
		} else {
			$this->cover_filename = $path . "_blank.png";
		}
	}
	
}

<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class Author extends Model {

	public $id;
	public $first_name;
	public $last_name;
	public $full_name;
	public $portrait_filename;

	public function __toString(): string {
		return $this->getFullName();
	}

	public function initialize(): void {
		$this->setSource("authors");

		$this->belongsTo("language_id", Language::class, "id", ["alias" => "Language"]);
		$this->hasManyToMany("id", NovelAuthor::class, "author_id", "novel_id", Novel::class, "id", ["alias" => "Novels"]);
	}

	public function afterFetch(): void {
		$public = $this->getDI()->get("config")->application->publicDir;
		$path = $this->getDI()->get("config")->application->portraitsUri;

		if($this->portrait_filename && file_exists($public . $path . $this->portrait_filename)) {
			$this->portrait_filename = $path . $this->portrait_filename;
		} else {
			$this->portrait_filename = $path . "_blank.png";
		}
	}

	public function getFullName(): string {
		return $this->last_name .", ". $this->first_name;
	}
	
}

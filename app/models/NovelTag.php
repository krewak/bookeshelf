<?php

namespace Bookeshelf\Models;

use Phalcon\Mvc\Model;

class NovelTag extends Model {

	public $id;
	public $novel_id;
	public $tag_id;

	public function initialize(): void {
		$this->setSource("novel_tags");
		$this->belongsTo("novel_id", Novel::class, "id");
		$this->belongsTo("tag_id", Tag::class, "id");
	}
	
}

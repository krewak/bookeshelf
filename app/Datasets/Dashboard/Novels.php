<?php

namespace Bookeshelf\Datasets\Dashboard;

use Bookeshelf\Helpers\DateTimeTranslator;
use Bookeshelf\Datasets\Novels as BaseNovels;
use Phalcon\Mvc\Model;

class Novels extends BaseNovels {

	protected $batch_limit = 0;

	public function map(Model $model): array {
		return [
			"authors" => $this->serializeAuthorsCollection($model->authors),
			"cover" => $model->cover_filename,
			"device_name" => $model->device->name,
			"id" => $model->id,
			"pages" => $model->pages,
			"publication_year" => $model->publication_year,
			"read_at_realdate" => $model->read_at ? DateTimeTranslator::getRealDate($model->read_at) : "w trakcie czytania",
			"title" => $model->title,
		];
	}

}
<?php

namespace Bookeshelf\Tables\Dashboard;

use Bookeshelf\Tables\Table;

class Novels extends Table {

	protected $header = "Zarządzaj przeczytanymi książkami";

	protected function setColumns(): void {
		$this->insertNewColumn()
			->setImageSource("cover");

		$this->insertNewColumn()
			->setLabel("tytuł")
			->setStrongValue("title");

		$this->insertNewColumn()
			->setLabel("autorzy")
			->setList("authors");

		$this->insertNewColumn()
			->setLabel("rok wyd.")
			->setTextValue("publication_year");

		$this->insertNewColumn()
			->setLabel("l. stron")
			->setTextValue("pages");

		$this->insertNewColumn()
			->setLabel("data przeczytania")
			->setTextValue("read_at_realdate");
			
		$this->insertNewColumn()
			->setLabel("nośnik")
			->setTextValue("device_name");
	}

}

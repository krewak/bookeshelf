<?php

namespace Bookeshelf\Controllers\Responses;

use Bookeshelf\Datasets\Authors;
use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Datasets\Details\Author;
use Bookeshelf\Tables\Authors as AuthorsTableHeader;
use Bookeshelf\Tables\Table;

class AuthorsController extends AbstractResponseController {

	public function getDataset(): Dataset {
		return new Authors();
	}

	public function getDetails(): Dataset {
		return new Author();
	}

	public function getTableHeader(): Table {
		return new AuthorsTableHeader();
	}

}

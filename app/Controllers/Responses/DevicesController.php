<?php

namespace Bookeshelf\Controllers\Responses;

use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Datasets\Details\Device;
use Bookeshelf\Datasets\Devices;
use Bookeshelf\Tables\Table;
use Bookeshelf\Tables\Devices as DeviceTableHeader;

class DevicesController extends AbstractResponseController {

	public function getDataset(): Dataset {
		return new Devices();
	}

	public function getDetails(): Dataset {
		return new Device();
	}

	public function getTableHeader(): Table {
		return new DeviceTableHeader();
	}

}

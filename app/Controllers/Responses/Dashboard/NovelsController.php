<?php

namespace Bookeshelf\Controllers\Responses\Dashboard;

use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Datasets\Details\Dashboard\Novel;
use Bookeshelf\Datasets\Dashboard\Novels;
use Bookeshelf\Forms\AbstractForm as Form;
use Bookeshelf\Forms\NovelForm;
use Bookeshelf\Tables\Dashboard\Novels as NovelsTableHeader;
use Bookeshelf\Tables\Table;

class NovelsController extends AbstractDashboardResponseController {

	public function getDataset(): Dataset {
		return new Novels();
	}

	public function getDetails(): Dataset {
		return new Novel();
	}

	public function getTableHeader(): Table {
		return new NovelsTableHeader();
	}

	public function getForm(): Form {
		return new NovelForm();
	}

}

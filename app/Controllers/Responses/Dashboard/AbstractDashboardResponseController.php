<?php

namespace Bookeshelf\Controllers\Responses\Dashboard;

use Bookeshelf\Controllers\Responses\AbstractResponseController;
use Bookeshelf\Exceptions\ObjectNotFound;
use Phalcon\Http\Response;

abstract class AbstractDashboardResponseController extends AbstractResponseController {

	public function formAction(int $id): Response {
		$form = $this->getForm();

		try {
			$object = $this->getDataset()->getObjectById($id);
		} catch(ObjectNotFound $e) {
			return $this->response->setJsonContent(["error" => "Nie znaleziono obiektu."]);
		}

		$response = $form->getEditForm($object);

		return $this->response->setJsonContent($response);
	}

	public function editAction(int $id): Response {
		$field = "pages";
		$value = rand(100, 255);

		$object = $this->getDataset()->getObjectById($id);
		$object->$field = $value;
		$object->save();

		return $this->response->setJsonContent(["success" => "Zmieniono wartość " . $value]);
	}

}

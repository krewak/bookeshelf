<?php

namespace Bookeshelf\Controllers\Responses;

use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Datasets\Details\Language;
use Bookeshelf\Datasets\Languages;
use Bookeshelf\Tables\Languages as LanguagesTableHeader;
use Bookeshelf\Tables\Table;

class LanguagesController extends AbstractResponseController {

	public function getDataset(): Dataset {
		return new Languages();
	}

	public function getDetails(): Dataset {
		return new Language();
	}

	public function getTableHeader(): Table {
		return new LanguagesTableHeader();
	}

}

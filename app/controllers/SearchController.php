<?php

namespace Bookeshelf\Controllers;

use Bookeshelf\Interfaces\Searchable;
use Bookeshelf\Datasets\Search\Authors;
use Bookeshelf\Datasets\Search\Languages;
use Bookeshelf\Datasets\Search\Devices;
use Bookeshelf\Datasets\Search\Novels;
use Bookeshelf\Datasets\Search\Tags;
use Phalcon\Http\Response;

class SearchController extends Controller {

	protected $searched_phrase = "";

	protected function getSortedCollection(Searchable $repository) {
		$collection = $repository->findObjects($this->searched_phrase);

		usort($collection, function($a, $b) {
			return $a["title"] <=> $b["title"];
		});

		return $collection;
	}

	public function searchAction(string $phrase): Response {
		$this->searched_phrase = rawurldecode($phrase);

		$novels = $this->getSortedCollection(new Novels);
		$authors = $this->getSortedCollection(new Authors);
		$tags = $this->getSortedCollection(new Tags);
		$languages = $this->getSortedCollection(new Languages);
		$devices = $this->getSortedCollection(new Devices);

		$this->response->setJsonContent([
			"success" => true,
			"results" => [
				"novels" => [
					"name" => "Powieści",
					"results" => $novels,
				],
				"authors" => [
					"name" => "Autorzy",
					"results" => $authors,
				],
				"tags" => [
					"name" => "Tagi",
					"results" => $tags,
				],
				"languages" => [
					"name" => "Języki",
					"results" => $languages,
				],
				"devices" => [
					"name" => "Nośniki",
					"results" => $devices,
				],
			],
		]);

		return $this->response;
	}

}

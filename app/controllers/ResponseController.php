<?php

namespace Bookeshelf\Controllers;

use Bookeshelf\Interfaces\ResponseControllerInterface;
use Phalcon\Http\Response;

abstract class ResponseController extends Controller implements ResponseControllerInterface {

	public function tableAction(): Response {
		$table = $this->getTableHeader();
		$result = [
			"limit" => $this->getDataset()->getBatchLimit(),
			"header" => $table->getHeader(),
			"columns" => $table->getColumns(),
		];
		return $this->response->setJsonContent($result);
	}

	public function dashboardTableAction(): Response {
		$table = $this->getDashboardTableHeader();
		$result = [
			"limit" => $this->getDataset()->getBatchLimit(),
			"header" => $table->getHeader(),
			"columns" => $table->getColumns(),
		];
		return $this->response->setJsonContent($result);
	}

	public function listAction(): Response {
		$offset = $this->request->get("offset") ?: 0;
		$list = $this->getDataset()->get($offset);
		return $this->response->setJsonContent($list);
	}

	public function detailsAction(int $id): Response {
		$details = $this->getDetails()->getDetails($id);
		return $this->response->setJsonContent($details);
	}

	public function editAction(int $id): Response {
		$field = "pages";
		$value = rand(100, 255);

		$object = $this->getDataset()->getObjectById($id);
		$object->$field = $value;
		$object->save();

		return $this->response->setJsonContent(["success" => "Zmieniono wartość " . $value]);
	}

}

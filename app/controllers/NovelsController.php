<?php

namespace Bookeshelf\Controllers;

use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Datasets\Details\Novel;
use Bookeshelf\Datasets\Novels;
use Bookeshelf\Tables\Novels as NovelsTableHeader;
use Bookeshelf\Tables\DashboardNovels;
use Bookeshelf\Tables\Table;

class NovelsController extends ResponseController {

	public function getDataset(): Dataset {
		return new Novels();
	}

	public function getDetails(): Dataset {
		return new Novel();
	}

	public function getTableHeader(): Table {
		return new NovelsTableHeader();
	}

	public function getDashboardTableHeader(): Table {
		return new DashboardNovels();
	}

}

<?php

namespace Bookeshelf\Controllers;

use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Datasets\Details\Tag;
use Bookeshelf\Datasets\Tags;
use Bookeshelf\Tables\Table;
use Bookeshelf\Tables\Tags as TagsTableHeader;

class TagsController extends ResponseController {

	public function getDataset(): Dataset {
		return new Tags();
	}

	public function getDetails(): Dataset {
		return new Tag();
	}

	public function getTableHeader(): Table {
		return new TagsTableHeader();
	}

}

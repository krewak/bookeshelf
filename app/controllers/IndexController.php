<?php

namespace Bookeshelf\Controllers;

use Phalcon\Http\Response;

class IndexController extends Controller {

	public function indexAction(): Response {
		$this->response->setJsonContent([
			"status" => true,
			"message" => "API is working"
		]);
		return $this->response;
	}

	public function notFoundAction(): Response {
		$this->response->setJsonContent([
			"status" => false,
			"message" => "URL not found"
		]);
		return $this->response;
	}

	public function noAccessAction(): Response {
		$this->response->setJsonContent([
			"status" => false,
			"message" => "URL not allowed"
		]);
		return $this->response;
	}

}

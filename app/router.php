<?php

use Phalcon\Mvc\Router;

$router = new Router(false);
$router->removeExtraSlashes(true);
$router->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);

$authenticated = !is_null($di->get("session")->get("auth"));

function r(string $action, string $controller, string $namespace = "Bookeshelf\\Controllers"): array {
	return [
		"namespace" => $namespace,
		"controller" => $controller,
		"action" => $action
	];
}

function ac(array $route_array, bool $allowed_for_authenticated): array {
	return $allowed_for_authenticated ? $route_array : r("noAccess", "Index");
}

$router->notFound(r("notFound", "Index"));

$router->addGet("/api", r("index", "Index"));

$router->addPost("/api/auth", r("check", "Authentication"));
$router->addPost("/api/login", ac(r("login", "Authentication"), !$authenticated));
$router->addPost("/api/logout", ac(r("logout", "Authentication"), $authenticated));

$router->addGet("/api/search/{phrase}", r("search", "Search"));

$rest_modules = [
	["route" => "novels", "controller" => "Novels"],
	["route" => "authors", "controller" => "Authors"],
	["route" => "languages", "controller" => "Languages"],
	["route" => "tags", "controller" => "Tags"],
	["route" => "devices", "controller" => "Devices"],
];

$response_namespace = "Bookeshelf\\Controllers\\Responses";
$dashboard_namespace = "Bookeshelf\\Controllers\\Responses\\Dashboard";

foreach($rest_modules as $module) {
	$router->addGet("/api/table/".$module["route"], r("table", $module["controller"], $response_namespace));
	$router->addGet("/api/".$module["route"], r("list", $module["controller"], $response_namespace));
	$router->addGet("/api/".$module["route"]."/{id}", r("details", $module["controller"], $response_namespace));

	$router->addGet("/api/dashboard/table/".$module["route"], ac(r("table", $module["controller"], $dashboard_namespace), $authenticated));
	$router->addGet("/api/dashboard/".$module["route"], ac(r("list", $module["controller"], $dashboard_namespace), $authenticated));

	$router->addGet("/api/dashboard/".$module["route"]."/{id}", ac(r("form", $module["controller"], $dashboard_namespace), $authenticated));
}

return $router;

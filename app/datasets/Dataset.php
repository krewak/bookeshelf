<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Exceptions\ObjectNotFound;
use Bookeshelf\Interfaces\DatasetInterface;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Resultset\Simple;

abstract class Dataset implements DatasetInterface {

	protected $batch_limit = 12;

	protected function prepareData(Simple $data): array {
		$result = [];

		foreach($data as $object) {
			$result[] = $this->map($object);
		}
		
		return $result;
	}

	public function get(int $offset = 0): array {
		$model_query = $this->getModelQuery();

		if($this->getBatchLimit() > 0) {
			$model_query = $model_query->limit($this->batch_limit, $offset);
		}
		
		$model_query = $model_query->execute();

		return $this->prepareData($model_query);
	}

	public function getDetails(int $id): array {
		$object = $this->getModelClass()::find($id);
		return $this->prepareData($object);
	}

	public function getModelQuery(): Criteria {
		return $this->getModelClass()::query();
	}

	public function getObjectById(int $id): Model {
		$object = $this->getModelClass()::findFirst($id);
		if(is_bool($object)) {
			throw new ObjectNotFound;
		}

		return $object;
	}

	public function getBatchLimit(): int {
		return $this->batch_limit;
	}

}

<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Helpers\DateTimeTranslator;
use Bookeshelf\Models\Author;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;

class Authors extends Dataset {

	public function getModelClass(): string {
		return Author::class;
	}

	public function getModelQuery(): Criteria {
		return parent::getModelQuery()->orderBy("last_name");
	}

	public function map(Model $model): array {
		$last_novel = $model->novels->getLast();
		
		return [
			"first_name" => $model->first_name,
			"id" => $model->id,
			"language" => $model->language,
			"language_name" => $model->language->name,
			"last_name" => $model->last_name,
			"portrait" => $model->portrait_filename,
			"written_novels" => $model->novels->count(),
			"last_novel" => $last_novel->title,
			"last_novel_id" => $last_novel->id,
			"last_novel_date" => $last_novel ? ($last_novel->read_at ?: "w trakcie") : "w trakcie",
			"last_novel_dateforhuman" => $last_novel ? ($last_novel->read_at ? DateTimeTranslator::getDateForHuman($last_novel->read_at) : "w trakcie czytania") : "",
		];
	}

}
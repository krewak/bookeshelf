<?php

namespace Bookeshelf\Datasets\Details;

use Bookeshelf\Datasets\NovelsCompacted;
use Bookeshelf\Datasets\Tags;
use Phalcon\Mvc\Model;

class Tag extends Tags {

	public function map(Model $model): array {
		return [
			"id" => $model->id,
			"name" => "#".$model->name,
			"novels" => call_user_func(function() use($model) {
				$novels = new NovelsCompacted();
				return $novels->getByTag($model->id);
			}),
		];
	}

}
<?php

namespace Bookeshelf\Datasets\Details;

use Bookeshelf\Datasets\NovelsCompacted;
use Bookeshelf\Datasets\Languages;
use Phalcon\Mvc\Model;

class Language extends Languages {

	public function map(Model $model): array {
		return [
			"id" => $model->id,
			"name" => $model->name,
			"symbol" => $model->symbol,
			"written_novels" => call_user_func(function() use($model) {
				$novels = new NovelsCompacted();
				return $novels->getByOriginalLanguage($model->id);
			}),
			"read_novels" => call_user_func(function() use($model) {
				$novels = new NovelsCompacted();
				return $novels->getByLanguage($model->id);
			}),
		];
	}

}
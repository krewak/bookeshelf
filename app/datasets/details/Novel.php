<?php

namespace Bookeshelf\Datasets\Details;

use Bookeshelf\Datasets\Novels;
use Bookeshelf\Helpers\DateTimeTranslator;
use Phalcon\Mvc\Model;

class Novel extends Novels {

	public function map(Model $model): array {
		return [
			"authors" => $this->serializeAuthorsCollection($model->authors),
			"cover" => $model->cover_filename,
			"summary" => $model->summary,
			"id" => $model->id,
			"language" => $model->language,
			"device" => $model->device,
			"original_language" => $model->originalLanguage,
			"original_title" => $model->original_title,
			"pages" => $model->pages,
			"publication_year" => $model->publication_year,
			"read_at_realdate" => $model->read_at ? DateTimeTranslator::getRealDate($model->read_at) : "w trakcie czytania",
			"reread" => $model->reread ? "powtórka" : "",
			"tags" => $this->serializeTagsCollection($model->tags),
			"title" => $model->title,
		];
	}

	protected function serializeTagsCollection($tags): array {
		return array_map(function($tag) {
			return [
				"id" => $tag["id"],
				"label" => $tag["name"],
			];
		}, $tags->toArray());
	}

}
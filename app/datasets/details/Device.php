<?php

namespace Bookeshelf\Datasets\Details;

use Bookeshelf\Datasets\NovelsCompacted;
use Bookeshelf\Datasets\Devices;
use Phalcon\Mvc\Model;

class Device extends Devices {

	public function map(Model $model): array {
		return [
			"id" => $model->id,
			"name" => $model->name,
			"novels" => call_user_func(function() use($model) {
				$novels = new NovelsCompacted();
				return $novels->getByDevice($model->id);
			}),
		];
	}

}
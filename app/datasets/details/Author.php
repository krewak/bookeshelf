<?php

namespace Bookeshelf\Datasets\Details;

use Bookeshelf\Datasets\Authors;
use Bookeshelf\Datasets\NovelsCompacted;
use Phalcon\Mvc\Model;

class Author extends Authors {

	public function map(Model $model): array {
		return [
			"first_name" => $model->first_name,
			"id" => $model->id,
			"language" => $model->language,
			"last_name" => $model->last_name,
			"portrait" => $model->portrait_filename,
			"novels" => call_user_func(function() use($model) {
				$novels = new NovelsCompacted();
				return $novels->getByAuthor($model->id);
			}),
		];
	}

}
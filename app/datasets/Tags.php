<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Models\Tag;
use Bookeshelf\Helpers\DateTimeTranslator;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;

class Tags extends Dataset {

	protected $batch_limit = 36;

	public function getModelClass(): string {
		return Tag::class;
	}

	public function getModelQuery(): Criteria {
		return parent::getModelQuery()->orderBy("name");
	}

	public function map(Model $model): array {
		$last_novel = $model->novels->getLast();

		return [
			"id" => $model->id,
			"name" => "#".$model->name,
			"tagged_novels" => $model->novels->count(),
			"last_novel" => $last_novel->title,
			"last_novel_id" => $last_novel->id,
			"last_novel_date" => $last_novel ? ($last_novel->read_at ?: "w trakcie") : "w trakcie",
			"last_novel_dateforhuman" => $last_novel ? ($last_novel->read_at ? DateTimeTranslator::getDateForHuman($last_novel->read_at) : "w trakcie czytania") : "",
		];
	}

}
<?php

namespace Bookeshelf\Datasets\Search;

use Bookeshelf\Models\Tag;
use Phalcon\Mvc\Model;

class Tags extends SearchDataset {

	protected $enable_images = false;

	public function getDatasetName(): string {
		return "tag";
	}

	public function getModelClass(): string {
		return Tag::class;
	}

	public function getLabelValue(Model $model): string {
		return $model->name;
	}

	public function getSublabelValue(Model $model): string {
		return "";
	}

	public function getWhereStatementForSearch(): string {
		return "name LIKE :phrase:";
	}

	public function getIconValue(): string {
		return "hashtag";
	}

}
<?php

namespace Bookeshelf\Datasets\Search;

use Bookeshelf\Models\Language;
use Phalcon\Mvc\Model;

class Languages extends SearchDataset {

	protected $enable_images = false;

	public function getDatasetName(): string {
		return "language";
	}

	public function getModelClass(): string {
		return Language::class;
	}

	public function getLabelValue(Model $model): string {
		return $model->name;
	}

	public function getSublabelValue(Model $model): string {
		return "";
	}

	public function getWhereStatementForSearch(): string {
		return "name LIKE :phrase:";
	}

	public function getIconValue(): string {
		return "globe";
	}

}
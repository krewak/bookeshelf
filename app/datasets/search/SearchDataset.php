<?php

namespace Bookeshelf\Datasets\Search;

use Bookeshelf\Exceptions\NotSearchableDataset;
use Bookeshelf\Interfaces\Searchable;
use Bookeshelf\Datasets\Dataset;
use Phalcon\Mvc\Model;

abstract class SearchDataset extends Dataset implements Searchable {

	protected $enable_images = true;

	public function map(Model $model): array {
		$result = [
			"dataset" => $this->getDatasetName(),
			"title" => $this->getLabelValue($model),
			"id" => $model->id,
			"description" => $this->getSublabelValue($model)
		];

		if($this->enable_images) {
			$result["image"] = $this->getImageValue($model);
		} else {
			$result["icon"] = $this->getIconValue();
		}

		return $result;
	}

	public function getImageValue(Model $model): string {
		return "";
	}

	public function getIconValue(): string {
		return "circle";
	}

	public function findObjects(string $phrase): array {
		if(!$this instanceof Searchable) {
			throw(new NotSearchableDataset());
		}

		$model_query = $this->getModelClass()::find([
			$this->getWhereStatementForSearch(),
			"bind" => [
				"phrase" => "%".$phrase."%",
			]
		]);

		return $this->prepareData($model_query);
	}

}

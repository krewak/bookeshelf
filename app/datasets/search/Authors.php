<?php

namespace Bookeshelf\Datasets\Search;

use Bookeshelf\Models\Author;
use Phalcon\Mvc\Model;

class Authors extends SearchDataset {

	public function getDatasetName(): string {
		return "author";
	}

	public function getModelClass(): string {
		return Author::class;
	}

	public function getLabelValue(Model $model): string {
		return $model->last_name;
	}

	public function getSublabelValue(Model $model): string {
		return $model->first_name;
	}

	public function getImageValue(Model $model): string {
		return $model->portrait_filename;
	}

	public function getWhereStatementForSearch(): string {
		return "first_name LIKE :phrase: OR last_name LIKE :phrase:";
	}

}
<?php

namespace Bookeshelf\Datasets\Search;

use Bookeshelf\Models\Novel;
use Phalcon\Mvc\Model;

class Novels extends SearchDataset {

	public function getDatasetName(): string {
		return "novel";
	}

	public function getModelClass(): string {
		return Novel::class;
	}

	public function getLabelValue(Model $model): string {
		return $model->title;
	}

	public function getSublabelValue(Model $model): string {
		return $model->original_title;
	}

	public function getImageValue(Model $model): string {
		return $model->cover_filename;
	}

	public function getWhereStatementForSearch(): string {
		return "title LIKE :phrase: OR original_title LIKE :phrase:";
	}

}
<?php

namespace Bookeshelf\Datasets\Search;

use Bookeshelf\Models\Device;
use Phalcon\Mvc\Model;

class Devices extends SearchDataset {

	protected $enable_images = false;

	public function getDatasetName(): string {
		return "device";
	}

	public function getModelClass(): string {
		return Device::class;
	}

	public function getLabelValue(Model $model): string {
		return $model->name;
	}

	public function getSublabelValue(Model $model): string {
		return "";
	}

	public function getWhereStatementForSearch(): string {
		return "name LIKE :phrase:";
	}

	public function getIconValue(): string {
		return "tablet";
	}

}
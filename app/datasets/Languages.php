<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Models\Language;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;

class Languages extends Dataset {

	public function getModelClass(): string {
		return Language::class;
	}

	public function getModelQuery(): Criteria {
		return parent::getModelQuery()->orderBy("name");
	}

	public function map(Model $model): array {		
		return [
			"id" => $model->id,
			"name" => $model->name,
			"symbol" => $model->symbol,
			"read_novels" => $model->readNovels->count(),
			"written_novels" => $model->writtenNovels->count(),
		];
	}

}
<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Models\Novel;
use Bookeshelf\Models\NovelAuthor;
use Bookeshelf\Models\NovelTag;
use Bookeshelf\Helpers\DateTimeTranslator;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;

class NovelsCompacted extends Novels {

	public function getByAuthor(int $id): array {
		$novel_class = Novel::class;
		$novel_authors_class = NovelAuthor::class;

		$model_query = $this->getModelQuery()
			->leftJoin($novel_authors_class, "$novel_class.id = ". "$novel_authors_class.novel_id") 
			->where("author_id = :id:", ["id" => $id])
			->execute();

		$repository = [];
		foreach($model_query as $novel) {
			$repository[] = $this->map($novel);
		}

		return $repository;
	}

	public function getByTag(int $id): array {
		$novel_class = Novel::class;
		$novel_tags_class = NovelTag::class;

		$model_query = $this->getModelQuery()
			->leftJoin($novel_tags_class, "$novel_class.id = ". "$novel_tags_class.novel_id") 
			->where("tag_id = :id:", ["id" => $id])
			->execute();

		$repository = [];
		foreach($model_query as $novel) {
			$repository[] = $this->map($novel);
		}

		return $repository;
	}

	public function getByLanguage(int $id): array {
		$model_query = $this->getModelQuery()
			->where("language_id = :id:", ["id" => $id])
			->execute();

		$repository = [];
		foreach($model_query as $novel) {
			$repository[] = $this->map($novel);
		}

		return $repository;
	}

	public function getByOriginalLanguage(int $id): array {
		$model_query = $this->getModelQuery()
			->where("original_language_id = :id:", ["id" => $id])
			->execute();

		$repository = [];
		foreach($model_query as $novel) {
			$repository[] = $this->map($novel);
		}

		return $repository;
	}

	public function getByDevice(int $id): array {
		$model_query = $this->getModelQuery()
			->where("device_id = :id:", ["id" => $id])
			->execute();

		$repository = [];
		foreach($model_query as $novel) {
			$repository[] = $this->map($novel);
		}

		return $repository;
	}

	public function map(Model $model): array {
		return [
			"cover" => $model->cover_filename,
			"id" => $model->id,
			"original_title" => $model->original_title,
			"pages" => $model->pages,
			"publication_year" => $model->publication_year,
			"read_at_dateforhuman" => $model->read_at ? DateTimeTranslator::getDateForHuman($model->read_at) : "",
			"read_at_realdate" => $model->read_at ? DateTimeTranslator::getRealDate($model->read_at) : "w trakcie czytania",
			"reread" => $model->reread ? "powtórka" : "",
			"title" => $model->title,
		];
	}

}
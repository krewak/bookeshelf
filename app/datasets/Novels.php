<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Helpers\DateTimeTranslator;
use Bookeshelf\Models\Novel;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Mvc\Model\Resultset\Simple;

class Novels extends Dataset {

	public function getModelClass(): string {
		return Novel::class;
	}

	public function getModelQuery(): Criteria {
		return parent::getModelQuery()->orderBy("read_at IS NOT NULL, read_at DESC");
	}

	public function map(Model $model): array {
		return [
			"authors" => $this->serializeAuthorsCollection($model->authors),
			"first_author_lastname" => $model->authors->getFirst()->last_name,
			"cover" => $model->cover_filename,
			"id" => $model->id,
			"language" => $model->language,
			"language_name" => $model->language->name,
			"device_id" => $model->device->id,
			"device_name" => $model->device->name,
			"original_language" => $model->originalLanguage,
			"original_language_name" => $model->originalLanguage->name,
			"original_title" => $model->original_title,
			"pages" => $model->pages,
			"publication_year" => $model->publication_year,
			"read_at" => $model->read_at ?: "w trakcie",
			"read_at_dateforhuman" => $model->read_at ? DateTimeTranslator::getDateForHuman($model->read_at) : "",
			"read_at_realdate" => $model->read_at ? DateTimeTranslator::getRealDate($model->read_at) : "w trakcie czytania",
			"reread" => $model->reread ? "ponownie czytana" : "",
			"title" => $model->title,
		];
	}

	protected function serializeAuthorsCollection(Simple $authors): array {
		return array_map(function($author) {
			return [
				"id" => $author["id"],
				"label" => $author["last_name"] . ", " . $author["first_name"],
			];
		}, $authors->toArray());
	}

}
<?php

namespace Bookeshelf\Datasets;

use Bookeshelf\Helpers\DateTimeTranslator;
use Bookeshelf\Models\Device;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;

class Devices extends Dataset {

	public function getModelClass(): string {
		return Device::class;
	}

	public function getModelQuery(): Criteria {
		return parent::getModelQuery()->orderBy("name");
	}

	public function map(Model $model): array {
		$last_novel = $model->novels->getLast();

		return [
			"id" => $model->id,
			"name" => "#".$model->name,
			"tagged_novels" => $model->novels->count(),
			"last_novel" => $last_novel->title,
			"last_novel_id" => $last_novel->id,
			"last_novel_date" => $last_novel ? ($last_novel->read_at ?: "w trakcie") : "w trakcie",
			"last_novel_dateforhuman" => $last_novel ? ($last_novel->read_at ? DateTimeTranslator::getDateForHuman($last_novel->read_at) : "w trakcie czytania") : "",
		];
	}

}
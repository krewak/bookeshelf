<?php

namespace Bookeshelf\Interfaces;

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Criteria;

interface DatasetInterface {

	public function getModelClass(): string;
	public function getModelQuery(): Criteria;
	public function map(Model $model): array;

}

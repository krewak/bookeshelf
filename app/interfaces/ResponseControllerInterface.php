<?php

namespace Bookeshelf\Interfaces;

use Bookeshelf\Datasets\Dataset;
use Bookeshelf\Tables\Table;

interface ResponseControllerInterface {

	public function getDataset(): Dataset;
	public function getDetails(): Dataset;
	public function getTableHeader(): Table;

}

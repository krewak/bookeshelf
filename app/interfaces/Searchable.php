<?php

namespace Bookeshelf\Interfaces;

use Phalcon\Mvc\Model;

interface Searchable {

	public function getDatasetName(): string;
	public function getWhereStatementForSearch(): string;
	public function getLabelValue(Model $model): string;
	public function getSublabelValue(Model $model): string;

}

<?php

namespace Bookeshelf\Exceptions;

use Exception;

class NotSearchableDataset extends Exception {

	public function __construct() {
		$this->message = "Only Dataset implementing Searchable interface is allowed.";
	}

}
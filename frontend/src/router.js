import Vue from "vue"
import Router from "vue-router"

import EventBus from "./event-bus.js"

import DataTable from "@/components/DataTable"
import Search from "@/components/Search"
import Login from "@/components/Login"
import NotFound from "@/components/NotFound"
import NotAllowed from "@/components/NotAllowed"

import Novel from "@/components/details/Novel"
import Author from "@/components/details/Author"
import Language from "@/components/details/Language"
import Detail from "@/components/details/Detail"

import Dashboard from "@/components/dashboard/Dashboard"
import ChangePassword from "@/components/dashboard/ChangePassword"
import DashboardDataTable from "@/components/dashboard/DashboardDataTable"
import EditItem from "@/components/dashboard/EditItem"

Vue.use(Router)

const router = new Router({
	mode: "history",
	routes: [
		{ path: "/", name: "home", redirect: { name: "novels" } },

		{ path: "/ksiazki", meta: { section: "novels" }, name: "novels", component: DataTable },
		{ path: "/ksiazka/:id", meta: { section: "novels" }, name: "novel", component: Novel },
		{ path: "/autorzy", meta: { section: "authors" }, name: "authors", component: DataTable },
		{ path: "/autor/:id", meta: { section: "authors" }, name: "author", component: Author },
		{ path: "/tagi", meta: { section: "tags" }, name: "tags", component: DataTable },
		{ path: "/tag/:id", meta: { section: "tags" }, name: "tag", component: Detail },
		{ path: "/jezyki", meta: { section: "languages" }, name: "languages", component: DataTable },
		{ path: "/jezyk/:id", meta: { section: "languages" }, name: "language", component: Language },
		{ path: "/nosniki", meta: { section: "devices" }, name: "devices", component: DataTable },
		{ path: "/nosnik/:id", meta: { section: "devices" }, name: "device", component: Detail },

		{ path: "/szukaj", meta: { section: "search" }, name: "search", component: Search },
		{ path: "/szukaj/:phrase", meta: { section: "search" }, name: "search-results", component: Search },

		{ path: "/logowanie", meta: { section: "login", requiresGuest: true }, name: "login", component: Login, },
		{ path: "/panel", meta: { section: "dashboard", requiresAuth: true }, name: "dashboard", component: Dashboard },
		{ path: "/panel/zmien-haslo", meta: { section: "dashboard", requiresAuth: true }, name: "password", component: ChangePassword },
		{ path: "/panel/ksiazki", meta: { section: "dashboard", requiresAuth: true }, name: "dashboard.novels", component: DashboardDataTable },
		{ path: "/panel/ksiazka/:id", meta: { section: "dashboard", requiresAuth: true }, name: "dashboard.novel", component: EditItem },

		{ path: "/brak-dostepu", meta: { section: "error" }, name: "not-allowed", component: NotAllowed },
		{ path: "*", meta: { section: "error" }, name: "not-found", component: NotFound },
	]
})

router.beforeEach((to, from, next) => {

	EventBus.$on("authentication_status", function(authenticationStatus) {
		if(to.matched.some(record => record.meta.requiresAuth)) {
			if(!authenticationStatus) {
				next({ name: "not-allowed" })
			}
		}

		if(to.matched.some(record => record.meta.requiresGuest)) {
			if(authenticationStatus) {
				next({ name: "not-allowed" })
			}
		}
	})

	next()
})

export default router

import Vue from "vue"

const EventBus = new Vue({
	data() {
		return {
			isAuthenticated: false
		}
	},
	created() {
		var self = this
		self.$on("authentication_status", function(isAuthenticated) {
			self.isAuthenticated = isAuthenticated
		})
	}
})

export default EventBus

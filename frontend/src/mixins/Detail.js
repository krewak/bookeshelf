import DetailTemplate from "../components/details/DetailTemplate"
import NovelsList from "../components/details/NovelsList"

export default {
	components: { 
		DetailTemplate,
		NovelsList
	},
	data() {
		return {
			dataset: "",
			item: null
		}
	},
	created() {
		var self = this
		self.dataset = self.$route.meta.section

		self.$http.get(this.apiUrl + self.dataset + "/" + self.$route.params.id).then(function(dataset) {
			if(dataset.body) {
				self.item = dataset.body[0]
				self.callbackAfterFetchingData()
			}
		})
	}, 
	methods: {
		callbackAfterFetchingData() {
			//
		}
	}
}
